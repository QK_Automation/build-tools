#!/bin/bash

SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $SCRIPTDIR/_commons

if [ -z "$BASE_HOSTNAME" ]; then
  INGRESS_IP="$(kubectl get service traefik-ingress -o "jsonpath={.status.loadBalancer.ingress[*].ip}")"
  BASE_HOSTNAME=${INGRESS_IP}.nip.io
fi

if [ -z "$INGRESS_IP" ] && [] -z "$BASE_HOSTNAME" ]; then
  printf "\ningress ip was not detected, have you executed \`setup-cluster\`? exiting.\n"
  exit 1
fi

# grafana / prometheus
wrap_helm repo add coreos https://s3-eu-west-1.amazonaws.com/coreos-charts/stable/
wrap_helm upgrade --install prometheus-operator coreos/prometheus-operator
wrap_helm upgrade --install \
  --set alertmanager.ingress.hosts={alertmanager.${BASE_HOSTNAME}} \
  --set prometheus.ingress.hosts={prometheus.${BASE_HOSTNAME}} \
  --set grafana.ingress.hosts={grafana.${BASE_HOSTNAME}} \
  --values helm/prometheus-values/values.yaml \
  kube-prometheus coreos/kube-prometheus 

# jaeger
wrap_helm repo add incubator https://kubernetes-charts-incubator.storage.googleapis.com/
wrap_helm upgrade --install \
  --set schema.activeDeadlineSeconds=600 \
  --set query.ingress.hosts={jaeger.${BASE_HOSTNAME}} \
  --values helm/jaeger-values/values.yaml \
  jaeger incubator/jaeger

echo "----------------------------------------------------------"
echo "   ingress url(s):"
echo "      alertmanager:  http://alertmanager.${BASE_HOSTNAME}"
echo "      grafana:       http://grafana.${BASE_HOSTNAME}"
echo "      jaeger:        http://jaeger.${BASE_HOSTNAME}"
echo "      prometheus:    http://prometheus.${BASE_HOSTNAME}"

