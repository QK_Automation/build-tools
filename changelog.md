8/13/19 - Major Updates
---

* Upgraded Helm and Kubernetes CLI client versions
* Created commons bash library for common stuff
* Removed dry-run capabilities from some things (never used)
* Switch to bash on multiple shell scripts because it was neccessary
* Added additional runtime descriptions on scripts
* Renamed gitlab settings get to fetch, because reasons
* Setup scripts now output dots to indicate work is underway
* Added check routines to eliminate unnecessary wait times
* Switched to single sub-domain model to ensure compatibility with legit star-certs
* Added wipe-monitoring script to only drop monitoring tools
* Switched from nginx-ingress to traefik because it's better
* Reduced CPU requirements for Jaeger since workshop has resource constraints
